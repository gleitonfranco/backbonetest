package br.com.hs.treino.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.hs.treino.model.Funcionario;

public class FuncionarioDao {
    private List<Funcionario> funcionarios;
    
    public FuncionarioDao() {
	funcionarios = new ArrayList<Funcionario>();
	funcionarios.add(new Funcionario(1L, "Gleiton") );
	funcionarios.add(new Funcionario(2L, "Ramiro") );
	funcionarios.add(new Funcionario(3L, "Lázaro") );
    }
    
    public Funcionario obterPorId(Long id) {
	for (Funcionario f : this.funcionarios) {
	    if (f.getId().equals(id)) {
		return f;
	    }
	}
	return null;
    }
    
    public List<Funcionario> obterTodos() {
	return this.funcionarios;
    }
    
    public String listaHtml() {
	String tag = "<table class='table' ><thead><th>ID</th><th>NOME</th></thead><tbody>";
	for (Funcionario f : this.funcionarios) {
	    tag = tag + "<tr><td>" + f.getId() + "</td><td>" + f.getNome() + "</td></tr>";
	}
	return tag + "</tbody></table>";
    }
}
