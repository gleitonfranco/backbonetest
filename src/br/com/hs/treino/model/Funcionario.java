package br.com.hs.treino.model;

public class Funcionario {
    public Long id;
    public String nome;

    public Funcionario(Long id, String nome) {
	this.id = id;
	this.nome = nome;
    }
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    
}
