package br.com.hs.treino.controller;

import java.util.List;

import com.google.inject.Provides;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.hs.treino.dao.FuncionarioDao;
import br.com.hs.treino.model.Funcionario;
//javax/servlet/jsp/jstl/core/Config 
@Resource
public class FuncionariosController {
    private Result result;
    private FuncionarioDao dao;
    
    public FuncionariosController(Result result, FuncionarioDao dao) {
	this.result = result;
	this.dao = dao;
    }
    
    public FuncionariosController(FuncionarioDao dao) {
	this.dao = dao;
    }
    
    public void lista() {
	this.result.use(Results.json()).from(dao.obterTodos()).serialize();
    }

    public void list() {
	this.result.include("list", dao.listaHtml());
    }
    
    public Funcionario form(Long id) {
//	this.result.include("id", 5L);
	Funcionario f = dao.obterPorId(id);
	System.out.println(f);
	return f;
    }

    @Get
    @Path("/funcionarios/{id}")
    public void obterPorId(Long id) {
//	return dao.obterPorId(id);
	this.result.use(Results.json()).from(dao.obterPorId(id)).serialize();
    }
}
