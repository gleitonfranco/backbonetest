<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Funcionários</title>
</head>
<body>
	<div class = "container jumbotron">
		<h1>Lista de Funcionários</h1>
		<i>${variable} ${linkTo[FuncionariosController].list}</i>
		<!-- ${list}<br> -->
		
		<table class="table table-dark" id="tbfunc">
			<thead><tr><td>ID</td><td>NOME</td><td>AÇÕES</td></tr></thead>
			<tbody>
			</tbody>
		</table>
	</div>

	 <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> 
	<!-- Backbone JS -->
	<script type="application/javascript" src="https://underscorejs.org/underscore-min.js"></script>
	<script type="application/javascript" src="https://backbonejs.org/backbone-min.js" ></script>
	<script type="application/javascript" src="/vraptor-blank-project/js/main.js" ></script>
	
</body>
</html>