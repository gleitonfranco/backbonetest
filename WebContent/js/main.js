var Funcionario = Backbone.Model.extend({
    
});

var FuncionarioList = Backbone.Collection.extend({
    model: Funcionario,
    url: 'http://localhost:8888/vraptor-blank-project/funcionarios/lista',
    parse: function(response) {
        return response.list;
    }
});

var FuncionarioRow = Backbone.View.extend({
    tagName: 'tr',
    
    model: Funcionario,
    
    collect: null,
    
//    initialize: function() {
//        this.list = $('#tbfunc');
//    },
    
    render: function() {
        this.$el.html('<td>'+this.model.attributes.id+'</td><td>'+this.model.attributes.nome
                +'</td><td><a href="http://localhost:8888/vraptor-blank-project/funcionarios/form?id='+this.model.attributes.id+'" class="btn btn-info" role="button">Editar</a>'
                +'</td><td><a href="#" class="btn btn-danger" role="button">Remover</a></td>');
        return this;
    }
});

var funcinarioList = new FuncionarioList();

var FuncionarioTable = Backbone.View.extend({

    views: null,
    tab: null,
    initialize: function() {
        this.views = $('#tbfunc tbody');
    },

    render: function() {
        this.tab.each( f => {
            var v = new FuncionarioRow({model: f});
            this.views.append(v.render().el);
        });
        return this;
    }
});

var a = null;
funcinarioList.fetch().done((o) => {
    console.log(o);
    a = new FuncionarioTable();
    a.tab = funcinarioList;
    a.render();
});
